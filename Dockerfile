FROM alpine:3.8

USER root

ENV HELM_VERSION v2.11.0
ENV KUBECTL_VERSION v1.11.2

RUN apk update
RUN apk add openjdk8 docker bash curl openssl git

#install helm
RUN curl https://storage.googleapis.com/kubernetes-helm/helm-$HELM_VERSION-linux-amd64.tar.gz | tar -xz -C /usr/local/bin --strip-components=1 linux-amd64/helm && chmod +x /usr/local/bin/helm
RUN helm init --client-only
RUN helm plugin install https://github.com/chartmuseum/helm-push
RUN helm repo add stable   	https://kubernetes-charts.storage.googleapis.com
RUN helm repo add incubator	http://storage.googleapis.com/kubernetes-charts-incubator

#install kubectl
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$KUBECTL_VERSION/bin/linux/amd64/kubectl
RUN mv kubectl /usr/local/bin/kubectl
RUN chmod +x /usr/local/bin/kubectl